# CHANGELOG

# 2.0.0

* feat: terraform 0.15 upgrade
* feat: (BREAKING) refactor account creation
* feat: Add possibility to create OU's
* chore: Update pre-commit configuration

# 1.0.0

* feat(BREAKING): terraform 0.12 upgrade

# 0.2.1

* chore: bump pre-commit
* chore: fix pre-commit
